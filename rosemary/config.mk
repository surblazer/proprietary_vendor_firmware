FIRMWARE_IMAGES := \
    audio_dsp \
    cam_vpu1 \
    cam_vpu2 \
    cam_vpu3 \
    gz \
    lk \
    md1img \
    preloader_raw \
    scp \
    spmfw \
    sspm \
    tee

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
